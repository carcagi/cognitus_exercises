// Definimos de donde vamos a traer la información.
let url = 'https://ghibliapi.herokuapp.com/films'; 

//Creamos un objeto de tipo XMLHttpRequest para manejar nuestras peticiones
let xmlhttp = new XMLHttpRequest();

// El siguiente codigo revisa cuando el estado del objeto cambia, si el estado es 4
// Y el status de respuesta es 200, entonces retorna un json = this.responseText
// https://www.w3schools.com/xml/ajax_xmlhttprequest_response.asp
// convertimos el json a un objeto sobre el que podamos iterar
// https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/JSON/parse
// y llamamos la función processInfo, y como parametro le mandamos el objeto 
xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        var myObject = JSON.parse(this.responseText);
        processInfo(myObject);
    }
};

// Esta es la linea que prepara la petición 
// parametros son metodo, url, y si es asincrono o no
// los metodos se pueden revisar aqui
// https://developer.mozilla.org/es/docs/Web/HTTP/Methods
xmlhttp.open('GET', url, true);

//Enviamos la petición
xmlhttp.send();

// Funcion que imprime el resultado en la consola
// En esta función se deben procesar la información
let processInfo = function(object){
	console.log(object);
}
